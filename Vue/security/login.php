<html>
<head>
    <?php
     include 'Vue/parts/global-css.php';
    ?>
    <link rel="stylesheet" href="Public/css/login-style.css">
</head>
<body>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Tabs Titles -->
        <h2>Me connecter</h2>
        <!-- Login Form -->
        <form method="post">
            <input type="text" id="login" class="fadeIn second" name="username" placeholder="login">
            <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
            <input type="submit" class="fadeIn fourth" value="Log In">
        </form>

        <?php
            require'Vue/parts/form-error.php';
        ?>

        <!-- Remind Passowrd -->
        <div id="formFooter">
            <a class="underlineHover" href="index.php?controller=security&action=register">Créer un compte</a>
        </div>

    </div>
</div>
</body>

<?php
include 'Vue/parts/global-scripts.php';
?>

</html>