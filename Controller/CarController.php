<?php
    class CarController {
        // Je déclare un attribut privé vehicule manager qui contiendra une instance
        // de ma classe manager (transforme le tableau clé valeur retourné par PDO en objet)
        private $vehiculeManager;

        // J'initialise mon attribut manager pour
        //qu'il soit une nouvelle instance de VehiculeManager
        public function __construct(){
            $this->vehiculeManager = new VehiculeManager();
        }

        // Fonction qui aficher tous nos véhicules. Elle retournera la vue de listing de nos véhicules
        public function listCar(){
            // J'appel mon manager pour lui demander de réccupérer toutes les voitures sous
            // forme de tableau d'objet
            $vehicules = $this->vehiculeManager->getAll();

            // J'appelle ma vue qui affichera toutes les voitures.
            require 'Vue/car/list.php';
        }


        // Fonction avec 2 cas d'utilisation différent
        // Dans le cas d'une requête de type GET, je vais afficher le formulaire d'ajout de véhicule
        // Dans le cas d'une requête de type POST, Je vais vérifier que tous les champs de mon formulaire sont OK
        // Si ils sont OK, j'enregistre mon véhicule puis je redirige l'utilisateur
        public function addAnnonce(){
            // Cette variable contient toutes les erreurs de mon formulaire
            $errors = [];
            // Cette variable contiendra les anciennes saisies utilisateurs
            $lastSaisie = [];

            // Ici je vérifie la méthode utilisée
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                // On vérifie la validité des informations saisies.
                $errors = $this->checkForm();
                $result = $this->uploadFile();

                $fileUrl = null;
                if($result['success']){
                    $fileUrl = $result['file_name'];
                }else {
                    $errors[] = $result['errors'];
                }

                // Si j'ai pas d'erreurs je vais créer un nouvel objet véhicule
                // J'appel mon manager pour insérer un nouvel objet véhicule
                if(count($errors) == 0){
                  $vehicule = new Vehicule($_POST['marque'], $_POST['modele'], $fileUrl);
                  $this->vehiculeManager->create($vehicule);

                  header('Location: index.php?controller=car&action=list');
                  exit();
                }
            }

            require 'Vue/car/add.php';
        }

        // Cette fonction prend en paramètre un ID. Cet id correspond à la ressource que l'on souhaite afficher
        public function detailCar($id){
            // On appel notre manager pour retrouver notre voiture
            $vehicule = $this->vehiculeManager->getOne($id);

            // Je vérifie que j'ai bien un vehicule
            if($vehicule != null){
                // Ok j'en ai un je l'affiche
                require 'Vue/car/detail.php';
            } else {
                // Je redirige mon utilisateur vers une page not found
                header('Location: index.php?controller=error&action=not-found&message=vehicule-not-found');
            }
        }

        // Fonction qui va supprimer un véhicule
        // Elle doit donc avoir l'id du véhicule à supprimer en paramètre
        public function deleteCar($id){
            // Ici on fait un get one pour vérifier que le véhicule existe bien
            $vehicule = $this->vehiculeManager->getOne($id);
            // Mon véhicule existe : Je vais le supprimer puis rediriger l'utilisateur
            if($vehicule != null){
                $this->vehiculeManager->delete($vehicule);
                header("Location: index.php?controller=car&action=list");
            } else {
                // Le cas ou il n'existe pas : j'affiche ma page d'erreur
                header('Location: index.php?controller=error&action=not-found&message=vehicule-not-found');
            }


        }


        // Cette fonction prendra un id en paramètre pour indiquer quel est l'élément à mettre à jour
        public function editCar($id){
            // Je déclare un tableau d'erreur vide
            $errors = [];
            // Je réccupére mon véhicule (celui ci n'est pas encore mis à jour)
            $vehicule = $this->vehiculeManager->getOne($id);

            if($_SERVER['REQUEST_METHOD'] == 'POST'){

                // Je rappel la fonction qui vérifie des champs de mes formulaires
                $errors = $this->checkForm();

                // Si j'éi pas d'erreurs je vais aller mettre à jour
                if(count($errors) == 0){
                    // Je cré un objet qui provient de mes variables POST + l'id du véhicule selectionné avant
                    $vehicule = new Vehicule($_POST['marque'], $_POST['modele'], $vehicule->getId());
                    // J'update le véhicule
                    $this->vehiculeManager->update($vehicule);
                    // Je redirige l'utilisateur
                    header('Location: index.php?controller=car&action=list');
                    exit();

                }
                // Il y a des erreurs, je raffiche alors mon formulaire avec la liste des erreurs
            }

            // J'affiche ma vue car/edit.php qui affichera le formulaire pré remplis
            require 'Vue/car/edit.php';
        }




        // Fonction qui va être appelée pour vérifier la validité d'un formulaire.
        // Elle controlera les données contenues dans ma variable super globale POST
        // Si il y a une erreur elle remplira un tableau qu'elle retournera.
        // Elle sera utilisé par nos fonctions addAnnonce et editCar
        private function checkForm(){
            $errors = [];
            if(empty($_POST['marque'])){
                $errors[] = 'Veuillez saisir une marque';
            } else {
                $lastSaisie['marque'] = $_POST['marque'];
            }

            if(empty($_POST['modele'])){
                $errors[] = 'Veuillez saisir un modèle';
            } else {
                $lastSaisie['modele'] = $_POST['modele'];
            }

            return $errors;
        }

        public function uploadFile(){

            $errors = [];
            $uploadExtension = ['image/jpeg', 'image/png'];

            if($_FILES['picture']['error'] != 0) {
                $errors[] = 'Erreur lors de l\'upload';
            }

            if(!in_array($_FILES['picture']['type'], $uploadExtension)){
                $errors[] = 'Veuillez ajouter une image png ou jpeg';
            }

            if($_FILES['picture']['size'] > 1000000){
                $errors[] = 'Fichier trop lourd';
            }



            if(count($errors) == 0){

                $name = uniqid().'.'.explode('/',$_FILES['picture']['type'])[1];

                move_uploaded_file($_FILES['picture']['tmp_name'], 'Public/uploads/'.$name);
                return ['success'=> true, 'file_name'=> 'Public/uploads/'.$name];
            } else {
               return ['success'=> false, 'errors'=> $errors];
            }

        }

    }
?>