<?php

// On inclut un fichier qui inclura toutes nos classes
// On verra par la suite comment faire plus propre
require 'include.php';

// On vérifie que l'on a pas de paramètres get dans l'url.
//Si c'est le cas, on redirige vers la page d'acceuil de notre application
if(!isset($_GET['controller']) || !isset($_GET['action'])){
    // Redirection vers la homepage
    header('Location: index.php?controller=car&action=list');
}

// Je regarde le controlleur. Si c'est un appel du securityController,
// Je cré un nouvel objet SecurityController
if ($_GET['controller'] == 'security') {

    $controller = new SecurityController();
    // Si mon parametre get action est égal à Login,
    // j'appel la fonction login de mon controlleur Security
    if( $_GET['action'] == 'login'){
        $controller->login();
    }
    // Ici j'appel la fonction register de mon objet SecurityController
    if ($_GET['action'] == 'register'){
        $controller->register();
    }
    // J'appel la fonction logout de mon SecurityController
    if ($_GET['action'] == 'logout'){
        $controller->logout();
    }

}

if ($_GET['controller'] == 'car' ) {

    // Ceci était un espace soumis à une authentification.
    // Ca implique que j'ai un attribut de session (user)
    if(empty($_SESSION) || !$_SESSION['user']){
        // Si je ne l'ai pas je redirige ver la page de login
        header('Location: index.php?controller=security&action=login');
    }

    $controller = new CarController();

    if($_GET['action'] == 'list'){
        // Pas de paramètre à passer dans la fonction
        // car on demande TOUTES les ressources
        $controller->listCar();
    }

    if($_GET['action'] == 'add'){
        // Pas de paramètres on est sur un formulaire d'ajout
        $controller->addAnnonce();
    }

    if($_GET['action'] == 'detail' && isset($_GET['id'])){
        // On passe le paramètre $_GET['id'];
        // Il servira à réccupérer le paramètre id que l'on souhaite afficher
        $controller->detailCar($_GET['id']);
    }

    if($_GET['action'] == 'delete' && isset($_GET['id'])){

        $controller->deleteCar($_GET['id']);
    }

    if($_GET['action'] == 'edit' && isset($_GET['id'])){
        $controller->editCar($_GET['id']);
    }
}

if($_GET['controller'] == 'test'){
    $controller = new TestController();
    $controller->helloWorld();
}

if($_GET['controller'] == 'error' && $_GET['action'] == 'not-found'){
    $controller = new ExceptionController();
    $controller->pageIntrouvable();
}



?>