<?php
interface UtilisateurManagerInterface {
    public function register(Utilisateur $utilisateur);
    public function login($username, $password);

}