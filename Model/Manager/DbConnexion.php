<?php
// Classe abstraite que sera en charge de se connecter à notre BDD
// Tous nos managers devront étendre de cette classe pour pouvoir effectuer des requêtes MySQL
abstract class DbConnexion
{

    // Attribut protected. Nos classes filles pourront donc se connecter à notre BDD
    protected $bdd;
    // Le hostname de notre BDD (chez vous localhost ou 127.0.0.1)
    private $host = 'database';
    // Le nom de notre BDD
    private $dbName = 'mvc_businesscase';
    // L'utilisateur de notre BDD
    private $username = 'root';
    // Le password de notre BDD
    private $password = 'tiger';

    // Cette fonction sera appelé lorsque l'on créra un nouveau manager. Elle permet de se
    // connecter à la BDD et stock notre BDD dans l'attribut protected bdd.
    // Nos manager utiliseront cet attribut pour requêter sur notre BDD
    public function __construct()
    {
        try {
            $this->bdd = new PDO('mysql:host=' . $this->host . '; dbname=' . $this->dbName, $this->username, $this->password, [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);
            $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            die('Erreur ' . $e->getMessage());
        }
    }
}