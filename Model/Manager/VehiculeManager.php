<?php

// Classe qui va permettre de transformer des resultats sql en objet
class VehiculeManager extends DbConnexion
{
    // J'appelle le constructeur de DbConnexion pour être connecté à ma BDD
    public function __construct()
    {
        parent::__construct();
    }

    // Fonction qui va selectionner TOUS nos véhicules et retourner un tableau d'objet
    // véhicule
    public function getAll()
    {
        $tableauVehicule = [];
        // Prépare la requête de selection
        $query = $this->bdd->prepare("SELECT * FROM vehicule;");
        $query->execute();
        // Réccupére les résultats
        $resultats = $query->fetchAll();

        // Transforme le tableau de résultat en tableau d'objet
        foreach ($resultats as $resultat) {
            // On passe ici par notre constructeur.
            $tableauVehicule[] = new Vehicule($resultat['marque'], $resultat['modele'], $resultat['image_link'], $resultat['id']);
        }

        return $tableauVehicule;
    }

    public function getOne($id)
    {
        $vehicule = null;

        // Je fais requête préparée pour selectionner un vehicule
        $query = $this->bdd->prepare("SELECT * FROM vehicule WHERE id = :id");
        // Je lui passe en paramètre l'id que je recherche
        $query->execute(['id' => $id]);

        // Je reccupére mon resultat sous forme de tableau
        $res = $query->fetch();

        // Si j'ai un resultat, je transforme mon tableau en objet
        if ($res) {
            $vehicule = new Vehicule($res['marque'], $res['modele'],$res['image'], $res['id']);
        }

        // Soit je n'ai pas de vehicule et mon retour sera : null
        // Sinon il contiendra mon objet utilisateur
        return $vehicule;
    }

    public function update(Vehicule $vehicule)
    {
        // Prépare une requête de mise à jour avec un nouvel objet
        $query = $this->bdd->prepare("UPDATE vehicule
         SET marque = :marque, modele = :modele WHERE id= :id ");

        // Elle met a jour l'objet
        $query->execute([
            'marque'=> $vehicule->getMarque(),
            'modele'=> $vehicule->getModele(),
            'id'=> $vehicule->getId()
        ]);
    }

    public function create(Vehicule $vehicule)
    {
        $query = $this->bdd->prepare("INSERT INTO vehicule (modele, marque, image_link) VALUES (:modele, :marque, :file)");
        $query->execute([
            'modele' => $vehicule->getModele(),
            'marque' => $vehicule->getMarque(),
            'file'=> $vehicule->getImage()
        ]);
    }

    public function delete(Vehicule $vehicule)
    {
        // Je prépare une requête de suppression
        $query = $this->bdd->prepare("DELETE FROM vehicule WHERE id = :id");
        // Je lui passe en paramètre l'id du véhicule à supprimer
        $query->execute(['id' => $vehicule->getId()]);
    }
}

?>