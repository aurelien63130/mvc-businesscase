<?php


// Nos classes représentent les champs de notre BDD sous forme d'objet.
// Nous aurons en général 1 attribut pour chaque colonne
class Utilisateur
{
    private $id;
    private $nom;
    private $prenom;
    private $email;
    private $username;
    private $password;

    // Nous permettra de créer nos objets. Nous passons un paramètre optionnel (id)
    // Il est optionnel car dans certains cas c'est la BDD qui nous le générera
    public function __construct($nom, $prenom, $email,
                                $username,
                                $password, $id = null
    )
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
        $this->username = $username;
        $this->password = $password;
    }


    // Toutes les méthodes publiques qui nous permettront de modifier et réccupérer
    // les valeurs de nos attributs privés.
    /**
     * @return mixed|null
     */
    public function getId(): mixed
    {
        return $this->id;
    }

    /**
     * @param mixed|null $id
     */
    public function setId(mixed $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }
}