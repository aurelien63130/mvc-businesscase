<?php
// Nos classes représentent les champs de notre BDD sous forme d'objet.
// Nous aurons en général 1 attribut pour chaque colonne
    class Vehicule{
        private $id;
        private $marque;
        private $modele;
        private $image;




        public function __construct($marque, $modele, $image = null, $id=null){
            $this->id = $id;
            $this->marque = $marque;
            $this->modele = $modele;
            $this->image = $image;
        }

        public function getId(){
            return $this->id;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function setMarque($marque){
            $this->marque = $marque;
        }

        public function getMarque(){
            return $this->marque;
        }


        /**
         * @return mixed
         */
        public function getModele()
        {
            return $this->modele;
        }

        /**
         * @param mixed $modele
         */
        public function setModele($modele): void
        {
            $this->modele = $modele;
        }

        /**
         * @return mixed|null
         */
        public function getImage(): mixed
        {
            return $this->image;
        }

        /**
         * @param mixed|null $image
         */
        public function setImage(mixed $image): void
        {
            $this->image = $image;
        }
    }
?>