<?php
    // Ici je retrouve TOUTES les classes
    // On met toujours nos classes objet avant les managers.
    // Si on a des interfaces on les placera avant la classe
    // Nous devons bien inclure le fichier DbConnexion avant nos managers
    // Nos en managers en hériteront donc il doit absolument être inclu avant


spl_autoload_register(function ($class_name) {
    $folders = [
        'Model/Class/',
        'Controller/',
        'Model/Manager/Interfaces/',
        'Model/Manager/'
    ];

    foreach ($folders as $folder){
        if(file_exists($folder.$class_name.'.php')){
            require $folder.$class_name.'.php';
            return;
        }
    }
});
?>